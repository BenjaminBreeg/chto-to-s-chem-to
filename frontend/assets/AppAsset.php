<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'bootstrap/css/bootstrap.min.css',
        'css/build/main.css',
        'css/flowplayer-custom.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
        'slick/slick/slick.css',
        'css/jquery-ui.min.css'
    ];
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js',
        'http://releases.flowplayer.org/6.0.5/flowplayer.min.js',
        'bootstrap/js/bootstrap.min.js',
        'slick/slick/slick.min.js',
        'js/jquery-ui.min.js',
        'js/jquery.jcarousel.min.js',
        'js/jquery.jcarousel-pagination.min.js',
        'js/jquery.jcarousel-control.min.js',
        'js/jquery.ui.touch-punch.min.js',
        'js/jquery.mobile-events.min.js',
        'js/functions.js',
        'js/chat.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
