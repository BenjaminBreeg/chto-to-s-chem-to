<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php Yii::$app->language ?>">
<head>
    <meta charset="<?php Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php Html::csrfMetaTags() ?>
    <title><?php Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="display-height">
<?php $this->beginBody() ?>

<div id="wrap" class="wrap">
    
    <header class='header'>
        <div class="header-inside">
            <div class="container-header w100">

                <div class="row">
                    <div class="col-xs-4 hidden-xs hidden-sm hidden-md hidden-lg">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-target="#rightmenu"  data-toggle="offcanvas" data-canvas="body">
                                <i class="glyphicon glyphicon-menu-hamburger"></i>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div id="rightmenu" class="navmenu navmenu-default navmenu-fixed-right offcanvas-md">
                            <ul class="nav navbar-nav">
                                <li><a href="#">ВАША СТРАНИЦА</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle navdrop">ИНФОРМАЦИЯ<b class="caret"></b></a>
                                    <div class="dropdown-menu noclose" >
                                        <ul class="container nav-tabs" role="tablist">
                                            <li><a href="#info-news" role="tab" data-toggle="tab">Новости</a></li>
                                            <li><a href="#info-question" role="tab" data-toggle="tab">Вопросы и Ответы</a></li>
                                            <li><a href="#info-rules" role="tab" data-toggle="tab">Правила</a></li>
                                            <li><a href="#info-instructions" role="tab" data-toggle="tab">Инструкция по
        <br>настройке оборудования</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="#" >СТАТИСТИКА</a></li>
                                <li class="dropdown open" >
                                    <a href="#" class="dropdown-toggle navdrop">НАСТРОЙКИ<b class="caret"></b></a>
                                    <div class="dropdown-menu  noclose">
                                        <ul class="container nav-tabs" role="tablist">
                                            <li><a href="#change1" role="tab" data-toggle="tab">Настройки оборудования</a></li>
                                            <li><a href="#change2" role="tab" data-toggle="tab">Изменить анкету</a></li>
                                            <li class="active"><a href="#change3" role="tab" data-toggle="tab">Изменить метод оплаты</a></li>
                                            <li><a href="#change4" role="tab" data-toggle="tab">Изменить E-mail<br> и паспортные данные</a></li>
                                            <li><a href="#change5" role="tab" data-toggle="tab">Сменить пароль</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="#">СВЯЗЬ С АДМИНИСТРАЦИЕЙ</a></li>
                                <li><a href="#">ДОБАВИТЬ НОВОГО УЧАСТНИКА</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                    <div class="col-xs-5 col-sm-3 col-md-3 col-lg-3">
                        <a href="/" class="pull-left header-logo">
                            <img src="img/logo.png" height="41" width="136" alt="" class="" title="Главная страница Sosedi">
                        </a>
                    </div>
                    <div class="col-xs-7 col-sm-9 col-md-9 col-lg-9">
                        <div class="menu pull-right">
                            <div class="menu-button hide menu-link">
                                <i class="fa fa-sign-out"></i>
                            </div>

                            <div class="header-inside-sections menu-content">
                                <div class="dropped" data-dropdown-layout=".header-inside-sections">
                                    <?php
//                                        if(Yii::$app::==='Login')
//                                        {
                                            if (Yii::$app->user->isGuest) {
                                                echo('<a href="');
                                                echo(Url::to(['/site/login']));
                                                echo('" class="header-inside-section header-inside-section-name">');
                                                echo('<span class="sprite-box"><i class="fa fa-user"></i></span>');
                                                echo('<span class="link-title">Войти');
                                                echo('</a><!-- END header-inside-section -->');
                                            } else {
                                                echo('<a href="#" class="header-inside-section header-inside-section-name dropdown-link menu-item">');
                                                echo('<span class="sprite-box"><i class="fa fa-user"></i></span>');
                                                echo('<span class="link-title">ЗДРАВСТВУЙТЕ<i class="fa fa-caret-down dropdown-caret"></i><span class="link-title-name">Guardian angel</span></span>');
                                                echo('</a><!-- END header-inside-section -->');

                                                echo('<ul class="dropped-menu dropdown-content">');
                                                echo('<li>');
                                                echo('<a href="#"><i class="sprite"><img src="img/header/login.png" height="20" width="20" alt=""></i><i class="sprite-hover"><img src="img/header/login-hover.png" height="20" width="20" alt=""></i>Ваш кабинет</a>');
                                                echo('</li>');
                                                echo('<li>');
                                                echo('<a href="#"><i class="fa fa-sign-out"></i>Выход</a>');
                                                echo('</li>');
                                                echo('</ul>');
                                            }
//                                        }
                                    ?>
                                </div><!-- END dropped -->
                                    <?php
                                        if(Yii::$app->user->isGuest)
                                        {
                                            echo('<a href="');
                                            echo(Url::to(['/site/signup']));
                                            echo('" class="header-inside-section menu-item">');
                                            echo("<span class='sprite-box'><i class='fa fa-pencil-square-o'></i></span>");
                                            echo("<span class='link-title'>Регистрация<span class=''>Создать учетную запись</span></span></a>");
                                        }
                                        else
                                        {
                                            echo("<a href='#' class='header-inside-section menu-item js-hide play'>");
                                            echo("<span class='link-title play-link-title'>НАЧАТЬ<span class=''>Трансляцию</span></span>");
                                            echo("<span class='link-title pause-link-title'>ОСТАНОВИТЬ<span class=''>Трансляцию</span></span>");
                                            echo("<span class='sprite-box play-sprite-box'><i class='fa fa-play'></i></span>");
                                            echo("<span class='sprite-box pause-sprite-box'><i class='fa fa-pause'></i></span>");
                                        }
                                    ?>
<!--                            </a>-->
                                <?php
                                     if(Yii::$app->user->isGuest != true)
                                     {
                                         echo("<div class='header-inside-section info-block' ><span class='sprite-box' ><i class='fa fa-money' ></i >");
                                         echo("</span ><span class='link-title' > ВАШ БАЛАНС <span class=''> На счету 100 монет </span ></span ></div>");
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- END header-inside -->
    </header>
<?= Alert::widget() ?>
<?= $content ?>
</div>
    <footer class="footer">
        <div class="footer-nav">
            <div class="container-footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-10 padding0 footer-col">
                        <ul class="nav nav-list">
                            <li><a href="#">Пользовательское соглашение</a></li>
                            <li><a href="#">Конфиденциальность</a></li>
                        </ul>
                        <ul class="nav nav-list">
                            <li><a href="#">Помощь</a></li>
                            <li><a href="#">Регистрация участника</a></li>
                        </ul>
                    </div>

                    <div class="hidden-xs col-sm-2 paddin-0 footer-logo-box">
                        <a href="/">
                            <img src="img/logo.png" alt="#" class="footer-logo big-logo">
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- END footer-nav -->

        <div class="footer-copyright text-center">
            <div class="row">
                <div class="copyright col-xs-9 col-sm-12 col-md-12 col-lg-12">
                    Copyright 2014 Sosedi.tv
                </div>
                <div class="text-right col-xs-3 hidden-sm hidden-md hidden-lg">
<!--                    todo fix url -->
                    <a href="/frontend/web/">
                        <img src="img/small-logo.png" alt="#" class="footer-logo small-logo">
                    </a>
                </div>
            </div>
        </div>
    </footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
