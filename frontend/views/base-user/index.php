<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Base Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="base-user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Base User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'auth_key',
            'password_hash',
            'password_reset_token',
            // 'email:email',
            // 'is_payed:boolean',
            // 'name',
            // 'surname',
            // 'fathername',
            // 'age',
            // 'country',
            // 'city',
            // 'education',
            // 'lastEducat',
            // 'profession',
            // 'workplace',
            // 'workflow?',
            // 'familyStatus',
            // 'child',
            // 'talants',
            // 'badHabbits',
            // 'advantages',
            // 'disadvantages',
            // 'altersexMainful',
            // 'why',
            // 'video',
            // 'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
