<?php
/**
 * User: whisper
 */
namespace frontend\widgets\camera;
use yii\web\AssetBundle;
class CameraAsset extends AssetBundle
{
	public $sourcePath = '@frontend/widgets/camera/assets';
	public $css = ['@frontend/widgets/camera/assets/css'];
	public $js = ['@frontend/widgets/camera/assets/js',];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}