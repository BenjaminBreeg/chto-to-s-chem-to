$(document).ready(function () {
    initMenu('.menu');
    initDropDown('.dropped');


    initImage();

    var myEfficientFn = debounce(function () {
        initImage();


    }, 250);


    $(window).resize(myEfficientFn);
    myEfficientFn();

    /**
     * смена play pause
     */
    $('.js-hide').click(function () {

        if ($(this).hasClass('play')) {
            $(this).removeClass('play');
            $(this).addClass('pause');
        }
        else {
            $(this).removeClass('pause');
            $(this).addClass('play');
        }
    });

    /**
     * запрет на закрыте dropdown bootstrap
     */
    $('.navdrop').click(function () {
        // если список открыт, то закрыть его
        // иначе открыть его
        $('.dropdown').removeClass('open');
        $(this).parent().addClass('open');
    });


    // смена способов оплаты
    $('.js-checkbox').change(function () {

        var id = $(this).attr("id");

        $("input.js-checkbox:checked").each(function () {
            if ($(this).attr("id") != id) $(this).attr("checked", false);

        });

        if ($('#webmoney').prop("checked")) {
            $('#webmoney-num').show();
            $('#payoneer-num').hide();
        }
        else if ($('#payoneer').prop("checked")) {
            $('#webmoney-num').hide();
            $('#payoneer-num').show();
        }
        else {
            $('#webmoney-num').hide();
            $('#payoneer-num').hide();
        }
    })

});

/**
 * Определение мобильного устройства
 */
function isMobile() {
    return navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i)
}

/**
 * Инициализация выпадающих списков
 */
function initDropDown(elements) {
    $(elements).each(function (index, el) {
        var layout = $(el).closest($(el).data('dropdown-layout'));

        $('.dropdown-link', el).on('click', function () {
            $('[data-dropdown-group=' + $(el).data('dropdown-group') + ']', layout).not(el).removeClass('active');

            $(el).toggleClass('active');
            return false;
        });

        $(layout).on('mouseleave', function () {
            $(el).removeClass('active');
        });

        $(layout).on('click', function () {
            $(el).removeClass('active');
        });
    });
}

/**
 * Инициализация меню
 */
function initMenu(elements) {
    $(elements).each(function (index, el) {
        $('.menu-link', el).on('click', function () {
            $(el).toggleClass('active');
            return false;
        });

        $('.menu-item', el).on('click', function () {
            // if ($('body').width() >= 960) {
            $(this).addClass('actives').delay(100).queue(function (next) {
                $(this).removeClass('actives');
                next();
            });
            // }
        });
    });

    $(document).on(isMobile() ? 'touchend' : 'click', function (e) {
        var parent = $(e.target).closest(elements);

        if (!parent.length) {
            $(elements).removeClass('active');
        }
    });
}


function initImage() {

    var w = $(".main-col img").width();
    var h = w / 16 * 9
    console.log(w, h, w / h)
    $(".main-col img").height(w / 16 * 9);
}

$(document).ready(function () {

    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };

    }

    $(".navbar-toggle").on('click', function () {
        $(this).toggleClass("clicked");

        if (!$(this).hasClass('clicked')) {
            $("#rightmenu").hide();
        } else {
            $("#rightmenu").show();
        }

        $(".menu.pull-right").removeClass("active");
    });

    $(".menu-button.menu-link").on('click', function () {
        $("#rightmenu").hide();
        $(".navbar-toggle").removeClass("clicked");
    });
    /*
     $("#rightmenu").height($(".main-box").height() + 52);

     $(window).resize(function(){
     $("#rightmenu").height($(".main-box").height() + 52);
     });*/
    //

    $(document).on('click touchstart', function (e) {
        if ($(".navbar-toggle").hasClass('clicked') && $(".navbar-toggle").is(":visible") && !$(e.target).closest('.clicked').length && !$(e.target).closest("#rightmenu").length) {
            $(".navbar-toggle").removeClass('clicked');
            $("#rightmenu").hide();
        }
        e.stopPropagation();
    });
});


function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };

}