<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="main-box main-custom" style="min-height: calc(100% - 237px);">
    <div class="container text-center">

        <form class="form" id="login-form" action="/frontend/web/index.php?r=site%2Flogin" method="post" role="form">

            <div class="form-group">
                <input type="text" class="form-control" id="loginform-username" name="LoginForm[username]" autofocus placeholder="Никнейм (Логин)">
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                <i class="fa fa-user"></i>
            </div>

            <div class="form-group nomargin">
                <input type="password" class="form-control" id="loginform-password" name="LoginForm[password]" placeholder="Пароль" >
                <i class="fa fa-lock"></i>
            </div>
            <p></p>
            <ul class="text-left check-wrapper">
                <li>
                    <input type="hidden" name="LoginForm[rememberMe]" value="0">
                    <input type="checkbox" id="loginform-rememberme"><label for="remeberMe" name="LoginForm[rememberMe]" value="1" checked>Запомнить меня</label>
                    <?php echo('<a href="');echo (Url::to(['site/request-password-reset'])); echo('" class="page-link pull-right">Напомнить пароль</a>'); ?>
                </li>
                <li class="empty"></li>
            </ul>
<!--            <button type="submit" class="page-button text-spacing" name="login-button">Вход</button>-->
                <?= Html::submitButton('Вход', ['class' => 'btn btn-primary page-button text-spacing', 'name' => 'login-button']) ?>
        </form>

    </div>
</div>

