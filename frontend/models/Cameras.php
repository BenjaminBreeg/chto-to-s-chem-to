<?php

namespace app\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "cameras".
 *
 * @property integer $id
 * @property string $user
 * @property string $type
 * @property string $hash
 */
class Cameras extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cameras';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'type'], 'required'],
            [['user'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 32],
            [['"user"'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user' => Yii::t('app', 'User'),
            'type' => Yii::t('app', 'Type'),
            'hash' => Yii::t('app', 'Hash'),
        ];
    }
    public static function getById($id)
    {
        return Cameras::findOne('id=id', array('id' => $id));
    }
    public static function getByUsername($username)
    {
        return Cameras::find('user=username', array('username' => $username));
    }
    function generateHash()
    {
        $hash = $this->user;
        $hash .= $this->id;
        return $hash;
    }
    public function getCameraURL()
    {
        $path = "http://localhost:8090/camera_";//todo make true settings for ffserver 
        $path .= $this->generateHash();
        $path .= "swf";
        return $path;
    }
}
