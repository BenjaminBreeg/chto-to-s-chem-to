<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_conf;
    public $check_license;
    public $news;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Такой пользователь уже существует'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Такой e-mail уже используется.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['password_conf', 'required'],
            ['password_conf', 'string', 'min' => 6],

            ['check_license', 'required'],
            ['check_license', 'boolean'],

            ['news', 'required'],
            ['news', 'boolean']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            foreach ($this->getErrors() as $key => $value)
            {
                echo $key . ': ' . $value[0];
            }
            return null;
        }
        if($this->password != $this->password_conf)
        {
            echo $this->password_conf;
            return null;
        }
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save();
    }
}
