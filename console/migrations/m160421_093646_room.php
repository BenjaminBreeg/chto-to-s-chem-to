<?php

use yii\db\Migration;

class m160421_093646_room extends Migration
{
    public function up()
    {
        $tableOptions = null;
        $this->createTable('{{%room}}', [
            'id' => $this->primaryKey(),
            'owner' => $this->string()->notNull()->unique(),
            'is_payed' => $this->boolean()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m160421_093646_room cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
