<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */


$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="main" class="clearfix">
<div class="header-title">
    <div class="vertical-center">РЕГИСТРАЦИЯ</div>
</div><!-- END header-title -->
<div class="main-box main-custom">
    <div class="container text-center">
                <?php $form = ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' => ['class' => 'form'],
                    'fieldConfig' => [
                        'template' => '<div class="form-group">{input}<i class="fa fa-user"></i></div>',
                    ]
                ]); ?>
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                    <?= $form->field($model, 'username') ->textInput(['class' => 'form-control', 'placeholder' => 'Никнейм (Логин)', 'autofocus' => true]) ?>
                    <?= $form->field($model, 'email')->textInput(['class' => 'form-control', 'placeholder' => 'E-mail']) ?>
                    <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => 'пароль']) ?>
                    <?= $form->field($model, 'password_conf')->passwordInput(['class' => 'form-control', 'placeholder' => 'Подтверждение пароля']) ?>
                <ul class="text-left check-wrapper">
                    <?= $form->field($model, 'check_license')->checkbox()?>
                    <?= $form->field($model, 'news')->checkbox()?>
<!--                    <li>-->
<!--                        <input type="checkbox" id="check_license"><label for="check_licence">Я принимаю условия <a href="#" class="page-link">Соглашения</a></label>-->
<!--                    </li>-->
<!--                    <li class="empty"></li>-->
<!--                    <li>-->
<!--                        <input type="checkbox" id="news"><label for="news">Новости? Мне интересно!</label>-->
<!--                    </li>-->
<!--                    <li class="empty"></li>-->
                </ul>
                <?= Html::submitButton('Signup', ['class' => 'page-button', 'name' => 'signup-button']) ?>
                <div class="clearfix"></div>
                <?php ActiveForm::end(); ?>
        </div>
        <p class="text-center">
        </p>
    </div>
    </div>
