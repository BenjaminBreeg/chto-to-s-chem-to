<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "base_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property boolean $is_payed
 * @property string $name
 * @property string $surname
 * @property string $fathername
 * @property integer $age
 * @property string $country
 * @property string $city
 * @property string $education
 * @property string $lastEducat
 * @property string $profession
 * @property string $workplace
 * @property string $workkind
 * @property string $familyStatus
 * @property string $child
 * @property string $talants
 * @property string $badHabbits
 * @property string $advantages
 * @property string $disadvantages
 * @property string $altersexMainful
 * @property string $why
 * @property string $video
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class BaseUser 
{
     /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'is_payed', 'created_at', 'updated_at'], 'required'],
            [['is_payed'], 'boolean'],
            [['age', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'name', 'surname', 'fathername', 'country', 'city', 'education', 'lastEducat', 'profession', 'workplace', 'workflow?', 'familyStatus', 'child', 'talants', 'badHabbits', 'advantages', 'disadvantages', 'altersexMainful', 'why', 'video'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'is_payed' => Yii::t('app', 'Is Payed'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'fathername' => Yii::t('app', 'Fathername'),
            'age' => Yii::t('app', 'Age'),
            'country' => Yii::t('app', 'Country'),
            'city' => Yii::t('app', 'City'),
            'education' => Yii::t('app', 'Education'),
            'lastEducat' => Yii::t('app', 'Last Educat'),
            'profession' => Yii::t('app', 'Profession'),
            'workplace' => Yii::t('app', 'Workplace'),
            'workflow?' => Yii::t('app', 'Workflow?'),
            'familyStatus' => Yii::t('app', 'Family Status'),
            'child' => Yii::t('app', 'Child'),
            'talants' => Yii::t('app', 'Talants'),
            'badHabbits' => Yii::t('app', 'Bad Habbits'),
            'advantages' => Yii::t('app', 'Advantages'),
            'disadvantages' => Yii::t('app', 'Disadvantages'),
            'altersexMainful' => Yii::t('app', 'Altersex Mainful'),
            'why' => Yii::t('app', 'Why'),
            'video' => Yii::t('app', 'Video'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
