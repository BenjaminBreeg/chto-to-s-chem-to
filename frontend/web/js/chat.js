(function ($) {
    var videos;
    var chat;
    var wrap;
    var slider;
    var video;
    var jcaruselSlideCount = 2;
    var current = 1;
    var marginBottom = 10;

    var jcar = null;

    // VIDEOS VIEW
    $(function () {
        videos = $('#js-videos');
        chat = $('#js-chat-container');
        wrap = $('.video-wrapper');
        slider = $('.video-slider');
        video = $('.video-box');

        $(window).resize(function () {
            updateSize(current)
        });

        // bind
        $('.js-view').on('click', videos, function (e) {
            e.preventDefault();
            var refresh = $(this).data('refresh');

            if (refresh) {
                $(".video-wrapper").sortable("refresh")
                $(".video-wrapper").sortable("refreshPositions")
            }

            current = $(this).data('view')
            $('.js-view').removeClass('active')
            $(this).addClass('active')

            updateSize(current)
        });

        $(".video-wrapper").sortable({
            connectWith: ".video-wrapper",
            items: ".js-drop:not(.ui-state-disabled)",
            handle: ".video-box-move",
            scroll: false
            //revert: true
        });
        $(".video-wrapper").disableSelection();

        updateSize(current)
    });


    function updateSize(view) {
        $('.b-content').addClass('ohidden');
        setSize(view);
        $('.b-content').removeClass('ohidden');
    }


    function setSize(n) {
        var height = 0;
        var width = 0;
        var sliderImageHeight = getHDHeight(getSliderWidth(videos));

        $('.b-content').removeClass('b-videos-1 b-videos-2 b-videos-4');
        $('.b-content').addClass('b-videos-' + current);

        var totalWidth = videos.width();
        var totalHeight = getHDHeight(totalWidth);

        wrap.width('auto');
        slider
            .width('auto')
            .height('auto')
            .css({
                'min-height': sliderImageHeight
            });


        if (scr().isTable()) {
            sliderImageHeight = 0
        }

        setChatHeight(totalHeight + sliderImageHeight + 10);
        videoDisable(current);

        var k = 0.7671;
        switch (n) {
            case 1:

                if (scr().isTable()) {
                    width = totalWidth * k;
                    height = getHDHeight(width);
                    totalHeight = height;
                    wrap.width(width);
                    slider.width(totalWidth - width - marginBottom).height(height);
                } else {
                    height = getHDHeight(totalWidth);
                    width = totalWidth;
                    slider.width(totalWidth);
                }


                break;
            case 2:
                var wrapWidth = 0

                if (scr().isMobile() || scr().isOldPhone()) {
                    height = getHDHeight(totalWidth);
                    width = totalWidth;
                    totalHeight = height * 2 + marginBottom * 3;
                } else {

                    //if (scr().isTable() || scr().isBigTable()) {
                    k = scr().isBigTable() ? 0.73 : 0.653;
                    k = scr().isDesktop ? 0.71 : k;
                    wrapWidth = totalWidth * k;
                    totalHeight = getHDHeight(wrapWidth) * 2 + marginBottom;
                    setChatHeight(totalHeight);
                    //} else {
                    //    totalHeight = totalHeight + sliderImageHeight + marginBottom;
                    //}


                    height = (totalHeight - marginBottom) / 2;
                    width = getHDWidth(height);
                    wrap.width(width);
                    slider.width(totalWidth - width - 10).height(totalHeight);

                    totalHeight = totalHeight - marginBottom;

                }

                break;
            case 4:

                if (scr().isTable()) {
                    var wrapWidth = totalWidth * k;
                    width = (wrapWidth - 10) / 2;
                    height = getHDHeight(width);
                    totalHeight = height * 2 + marginBottom;

                    slider.width(totalWidth - wrapWidth - 10).height(totalHeight)
                    wrap.width(wrapWidth);
                } else {
                    width = (totalWidth - 11) / 2;
                    height = getHDHeight(width);
                    slider.width(totalWidth);
                    totalHeight = height * 2 + marginBottom;
                    setChatHeight(totalHeight + sliderImageHeight + 10);
                }


                break
        }


        wrap.height(totalHeight);
        width = Math.floor(width);
        video
            .height(height)
            .width(width);


        initVideoSlider(slider, current);

    }

    function setChatHeight(height) {
        if (scr().isDesktop() || (scr().isTable() && current == 2 )) {
            chat.height(height)
        }
    }

    function initVideoSlider(slider) {
        var jconfig = {
            wrap: 'circular',
            relative: true
        };

        slider.find('.video-slider-box').height('auto');


        if (scr().isMobile() && current !== 2) {
            slider
                .width('auto')
                .height(174)
        } else if (scr().isMobile()) {
            slider
                .width('auto')
                .height(174)
        } else if (scr().isOldPhone()) {
            slider
                .width('auto')
                .height(115)
        }


        if (scr().isTable() || current == 2 && !(scr().isMobile() || scr().isOldPhone())) {
            initVerticalSlider(slider);
            slider.addClass('vert');
            jconfig = $.extend({}, jconfig, {vertical: true})
        } else {
            initHorizontalSlider(slider);
            slider.removeClass('vert');
            jconfig = $.extend({}, jconfig, {vertical: false})
        }

        if (!jcar) {
            jcar = $('.jcarousel').jcarousel(jconfig);

            $('.js-prev').jcarouselControl({
                target: '-=' + jcaruselSlideCount
            });

            $('.js-next').jcarouselControl({
                target: '+=' + jcaruselSlideCount
            });


            $('.jcarousel-pagination')
                .on('jcarouselpagination:active', 'a', function () {
                    $(this).addClass('active');
                })
                .on('jcarouselpagination:inactive', 'a', function () {
                    $(this).removeClass('active');
                })
                .on('click', function (e) {
                    e.preventDefault();
                })
                .jcarouselPagination({
                    perPage: 1,
                    item: function (page) {
                        return '<a href="#' + page + '">' + page + '</a>';
                    }
                });
        } else {
            jcar.jcarousel('reload', jconfig)
        }

        sliderDragOn()
    }

    function initVerticalSlider(slider) {

        var width = slider.width() - 25;
        var height = getHDHeight(width);

        slider.find('.video-slider-box').height(slider.height());

        $('.video-slider-slide', slider)
            .width(width)
            .height(height);

        slider.find('.slick-arrow').css({
            top: ''
        });
    }

    function initHorizontalSlider(slider) {
        var width = 0;
        var height = 0;

        if (scr().isOldPhone()) {
            width = 149;
            height = 89;
        } else if (scr().isMobile() || ((scr().isTable()) && current !== 2)) {
            width = 240;
            height = 135;
        } else {
            width = getSliderWidth(slider);
            height = getHDHeight(width);
        }

        $('.video-slider-slide', slider)
            .width(width)
            .height(height);

        var paddingTop = 5;
        if (current == 4 && scr().isDesktop()) {
            paddingTop = 15;
        }
        slider.find('.slick-arrow').css({
            top: height / 2 - paddingTop
        });
    }


    function sliderDragOn(value) {
        $(".video-slider-slide").draggable({
            appendTo: "body",
            helper: "clone",
            containment: ".b-videos",
            scroll: false,
            revert: true,
            zIndex: 10000
        });


        $(".js-drop").droppable({
            drop: function (event, ui) {
                console.log('Show video')
            }
        });
    }

    function sliderDragOff(value) {
        $(".video-slider-slide").draggable("destroy");
        $(".js-drop").droppable("destroy");


        $(".video-slider-slide").draggable({
            appendTo: "body",
            helper: "clone",
            cursor: "move",
            containment: ".b-videos",
            scroll: false,
            revert: true
        });

        $(".b-videos .slick-slide").off("draggable mouseenter mousedown", function (event) {
            event.stopPropagation();
        });

    }

    // UTILS
    function getSliderWidth(slider) {
        return (slider.width() - 30) / 4
    }

    function getHDHeight(width) {
        return Math.ceil(width / 16 * 9)
    }

    function getHDWidth(height) {
        return Math.ceil(height / 9 * 16)
    }

    function videoDisable(number) {
        var rule = {
            0: [1, 2, 3, 4],
            1: [2, 3, 4],
            2: [3, 4],
            4: []
        };

        $('.js-drop').removeClass('.ui-state-disabled').each(function (elm, key) {
            if (rule[number].indexOf(key) !== -1) {
                $(this).addClass('.ui-state-disabled')
            }
        });
        console.log('drop', number)
    }


    function scr() {
        var scrWidth = window.innerWidth;

        return {
            isMobile: function () {
                return scrWidth <= 768 && scrWidth > 480
            },
            isTable: function () {
                return scrWidth > 768 && scrWidth <= 1024
            },
            isBigTable: function () {
                return scrWidth > 1024 && scrWidth <= 1080; // 1024 1080
            },
            isDesktop: function () {
                return scrWidth > 1090; // 1024 1080
            },
            isOldPhone: function () {
                return scrWidth <= 480
            }
        }
    }


    // CHAT
    $(function () {
        initChatSlider();
        initChatResize();
    });


    function initChatSlider() {
        var chat = $("#js-chat");

        $('.js-chat-slide').on('click', chat, function () {
            chat.toggleClass('js-chat-show-users');
            dotAddClass()
        });

        $('.js-chat-open-users').on('click', chat, function () {
            chat.addClass('js-chat-show-users');
            dotAddClass()

        });

        $('.js-chat-open-messages').on('click', chat, function () {
            chat.removeClass('js-chat-show-users');
            dotAddClass()
        });

        $('#js-chat-container').on('swiperight', chat, function () {
            chat.removeClass('js-chat-show-users');
            document.getSelection().removeAllRanges();
            dotAddClass()
        }).on('swipeleft', chat, function () {
            chat.addClass('js-chat-show-users');
            document.getSelection().removeAllRanges();
            dotAddClass()
        });


        function dotAddClass() {
            var li = chat.find('.slick-dots>li');
            li.removeClass('slick-active');

            if (chat.hasClass('js-chat-show-users')) {
                li.get(1).classList.add('slick-active');
            } else {
                li.get(0).classList.add('slick-active');
            }
        }
    }

    function initChatResize(elements) {
        var chat = $("#js-chat");

        $('.js-chat-resize').on('click', chat, function (e) {
            e.preventDefault();
            chat.toggleClass('js-chat-big')
        })
    }


})(jQuery);