<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cameras */

$this->title = Yii::t('app', 'Create Cameras');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cameras'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- todo refactorize php open tag -->
<div class="cameras-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
