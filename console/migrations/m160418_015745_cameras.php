<?php

use yii\db\Migration;

class m160418_015745_cameras extends Migration
{
    public function up()
    {
        $tableOptions = null;
//        if ($this->db->driverName === 'pgsql') {
//            $tableOptions = 'CHARACTER SET utf8';
//        }

        $this->createTable('{{%cameras}}', [
            'id' => $this->primaryKey(),
            'user' => $this->string()->notNull()->unique(),
            'type' => $this->string(32)->notNull(),
            'hash' =>$this->string(32)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m160418_015745_cameras cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
