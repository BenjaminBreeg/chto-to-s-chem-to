<?php

namespace app\models;

use Yii;
use app\models\Cameras;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "room".
 *
 * @property integer $id
 * @property string $owner
 * @property string $is_payed
 */
class Room extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $id;
    public $owner;
    public $is_payed;
    public $cameras = array();
    public $members = array();

    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner', 'is_payed'], 'required'],
            [['owner'], 'string', 'max' => 255],
//            [['owner'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner' => Yii::t('app', 'Owner'),
            'is_payed' => Yii::t('app', 'Payable'),
        ];
    }
    public function appendCamera($camera)
    {
        foreach ($cameras as $cam)
            if($camera->id === $cam->id)
            {
                return false;
            }
        array_push($this->cameras, $camera);
        return true;
    }
    public function getCameraById($id)
    {
        if($this->cameras[$id])
        {
            return $this->cameras[$id];
        }
        else
        {
            return null;
        }
    }
    public function getCameraList()
    {
        return $this->cameras;
    }
    public function appendMember($member)
    {
        foreach ($this->members as $memb)
            if ($member === $memb)
            {
                return false;
            }
        array_push($this->members, $member);
        return true;
    }
    public function getMemberById($id)
    {
        if($this->members[$id])
        {
            return $this->members[$id];
        }
        else
        {
            return true;
        }
    }
    public function getMemberList()
    {
        return $this->members;
    }
}
